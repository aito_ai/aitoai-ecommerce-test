import chai               from 'chai'
import axios              from 'axios'

const environment = require('./environment.json')

const requestConfig = {
  headers: { 'x-api-key': environment['x-api-key']}
}

chai.should()

describe("Run queries against the aito.ai database populated with the example data", () => {
  it("Query the click for Apple iphone based on the impressions", done => {
    axios.post(`${environment.host}/api/_estimate`, require('./queries/estimate_apple_iphone_click.json'), requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.have.property('estimation')
        r.data.estimation.should.be.closeTo(0.18556, 0.01)
        done()
      })
      .catch(err => {
        done(err)
      })
  }),
  it("Predict the query by a given user based on the impressions", done => {
    axios.post(`${environment.host}/api/_predict`, require('./queries/predict_query_from_user.json'), requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.be.an('array')
        r.data[0].probability.should.be.closeTo(0.1689, 0.001)
        r.data[0].variable.feature.should.be.equal("phone")
        done()
      })
      .catch(err => {
        done(err)
      })
    }),
  it("Predict tags for a product", done => {
    axios.post(`${environment.host}/api/_predict`, require('./queries/non_exclusive_predict.json'), requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.be.an('array')
        r.data[0].probability.should.be.closeTo(0.88, 0.01)
        r.data[0].variable.feature.should.be.equal("cover")
        done()
      })
      .catch(err => {
        done(err)
      })
    }),
  it("Relate product with title when click is true", done => {
    axios.post(`${environment.host}/api/_relate`, require('./queries/relate_product_and_title.json'), requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.be.an('array')
        const first = r.data[0]
        first.mutualInformation.should.be.closeTo(0.0065, 0.0005)
        first.variables[0].frequency.should.be.equal(20)
        first.variables[0].field.should.be.equal("click")
        done()
      })
      .catch(err => {
        console.log(`Erroneous response => ${JSON.stringify(err.response.data, null, 2)}`)
        done(err)
      })
    }),
  it("Find products and then order by click probability", done => {
      axios.post(`${environment.host}/api/_find`, require('./queries/find_and_orderby_probability.json'), requestConfig)
        .then(r => {
          r.status.should.equal(200)

          const o = r.data
          o.rows.should.be.an('array')
          o.count.should.be.equal(11)
          const rows = o.rows
          rows[0].hitIndex.should.be.equal(0)
          rows[0].rowIndex.should.be.equal(3)
          rows[0].row.title.should.be.equal("apple macbook")
          rows[0].probability.should.be.closeTo(0.31, 0.01)
          done()
      })
      .catch(err => {
        done(err)
      })
    }),
  it("Find products and do triangle examination", done => {
    axios.post(`${environment.host}/api/_find`, require('./queries/find_doing_triangle_examination.json'), requestConfig)
      .then(r => {
        r.status.should.equal(200)

        const o = r.data
        o.rows.should.be.an('array')

        const rows = o.rows
        rows[0].hitIndex.should.be.equal(0)
        rows[0].rowIndex.should.be.equal(6)
        rows[0].row.title.should.be.equal("iphone protection white")
        rows[0].probability.should.be.closeTo(0.465, 0.01)
        done()
      })
      .catch(err => {
        done(err)
      })
    }),
  it("Reverse find: Customer interested in product", done => {
    axios.post(`${environment.host}/api/_find`, require('./queries/reverse_triangle_examination.json'), requestConfig)
      .then(r => {
        r.status.should.equal(200)

        const o = r.data
        o.rows.should.be.an('array')

        const rows = o.rows

        console.log(`${JSON.stringify(rows, null, 2)}`)
        rows[0].hitIndex.should.be.equal(0)
        rows[0].rowIndex.should.be.equal(0)
        rows[0].row.name.should.be.equal("anne")
        rows[0].probability.should.be.closeTo(0.851, 0.01)
        done()
      })
      .catch(err => {
        done(err)
      })
    })
})
