import chai               from 'chai'
import axios              from 'axios'

const environment = require('./environment.json')

const schema = require('./data/schema.json')
const customers = require('./data/customers.json')
const products = require('./data/products.json')
const impressions = require('./data/impressions.json')

const requestConfig = {
  headers: { 'x-api-key': environment['x-api-key']}
}

chai.should()

describe("Setup the aito.ai database from the provided data", () => {
  it("Deletes the current schema in the aito-database", done => {
    axios.delete(`${environment.host}/api/schema/_mapping`, requestConfig)
      .then(r => {
        r.status.should.equal(200)
        done()
      })
      .catch(err => {
        done(err)
      })
  }),


  it("Reads the schema file, and provisions it to the aito-database", done => {
    axios.put(`${environment.host}/api/schema/_mapping`, schema, requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.have.property('tables')
        const tables = r.data.tables
        tables.should.be.an('array')
        tables.should.have.lengthOf(3)
        tables[0].table.should.be.equal("customers")
        tables[1].table.should.be.equal("products")
        tables[2].table.should.be.equal("impressions")
        done()
      })
      .catch(err => {
        done(err)
      })
  }),

  it("Populates the customer data from the json file", done => {
    axios.post(`${environment.host}/api/data/customers/batch`, customers, requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.be.deep.include({
          'status': 'ok',
          'entries': 9
        })
        done()
      })
      .catch(err => {
        done(err)
      })
  }),

  it("Populates the product data from the json file", done => {
    axios.post(`${environment.host}/api/data/products/batch`, products, requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.be.deep.include({
          'status': 'ok',
          'entries': 11
        })
        done()
      })
      .catch(err => {
        done(err)
      })
  }),

  it("Populates the impresssions data from the json file", done => {
    axios.post(`${environment.host}/api/data/impressions/batch`, impressions, requestConfig)
      .then(r => {
        r.status.should.equal(200)
        r.data.should.be.deep.include({
          'status': 'ok',
          'entries': 253
        })
        done()
      })
      .catch(err => {
        done(err)
      })
  }),

  it("Query and verify the data the database", done => {
    const createQuery = name => {
      return {
        "context": name,
        "q": [
          {"find": {}},
          {"take": 1, "from": 0}
        ]
      }
    }

    for (let ctx of ["customers", "products", "impressions"]) {
      axios.post(`${environment.host}/api/_find`, createQuery(ctx), requestConfig)
        .then(r => {
          r.status.should.equal(200)
          r.data.should.be.deep.include({'count': 1})
        })
        .catch(err => {
          done(err)
        })
    }
    done()
  })

})
