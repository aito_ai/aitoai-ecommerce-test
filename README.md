# aitoai-ecommerce-test

This repository contains the ecommerce test cases described in the documentation project (https://bitbucket.org/aito_ai/aitoai-documentation).

The project shows how to query the aito.ai database, and what kind of responses to expect. The data in the example is purely fictional,
and has been created for demonstration purposes only.

Please feel free to comment the code and send us pull requests or issues, if you find problems with the code.

We are setting up an interactive environment where you could spawn your own instance to try out these examples. If you are interested
please follow [the instructions on getting in touch with us in the documentation](https://bitbucket.org/aito_ai/aitoai-documentation#markdown-header-11-feedback-reporting-bugs-or-inconsistencies).

This code is licensed under the MIT license, and you are free to copy it, modify it and distribute it as you see fit. 
